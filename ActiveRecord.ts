import { PgHelper } from './PgHepler';


export interface Row {
    [ key: string ]: any;
}

export interface Field {
    /**some fields */
}

export interface Fields {
    [ key: string ]: Field;
}

export class ActiveRecord {
    [key: string]: any;
    tableName: string;
    pkFieldName : string;
    pkSequenceName: string;
    tableFields: Fields;
    lastSql?:string;
    pginstance?: PgHelper;

    constructor() {
        this.tableName = this.getTableName();
        this.pkFieldName = this.getPkFieldName();
        this.pkSequenceName = this.getPkSequenceName();
        this.tableFields = this.getTableFields();
    }

    getTableName(): string {
        throw new Error("Not implemented getTableName()");
    }

    getPkFieldName(): string {
        throw new Error("not implemented getPkFieldName()");
    }

    getPkSequenceName(): string {
        return '';
    }

    getTableFields(): Fields {
        throw new Error("not implemented getTableFields()");
    };

    static async findByPk(pk_id: number | string, pginstance: PgHelper) {
        const ar = new this();
        const sql = `SELECT * FROM ${ar.tableName} where ${ar.pkFieldName} = $1`;
        ar.lastSql = sql;
        const sqlResult = await pginstance.query(sql, [pk_id]);
        if (sqlResult.rows.length) {
            ar.load(sqlResult.rows[0]);            
            ar.pginstance = pginstance;
        } 
        return ar;
    }

    async delete(pginstance?: PgHelper) {
        const sql = `DELETE FROM ${this.tableName} where ${this.pkFieldName} = $1`;
        this.lastSql = sql;
        await this._getPg(pginstance).query(sql, [ this[this.pkFieldName] ]);
    }

    async save(pginstance?: PgHelper) {
        
        if (this[this.pkFieldName]) {
            let updateSqlSection = '';
            let whereSqlSection = '';
            let values : object[] = [];
            Object.keys(this.tableFields).forEach((key,index) => {
                
                if (key === this.pkFieldName) {
                    whereSqlSection = `${this.pkFieldName} = $${index + 1}`
                    values.push(this[key]);
                } else if(this[key] || this[key] === 0 || this[key] === null ) {
                    updateSqlSection += key + '=' + `$${index + 1}` + ',';
                    values.push(this[key]);
                }
            });
            let sql = `UPDATE ${this.tableName}
                          SET ${updateSqlSection.slice(0,-1)} 
                        WHERE ${whereSqlSection}
                    RETURNING * `;
            this.lastSql = sql;
            const sqlResult = await this._getPg(pginstance).query(sql, values);
            this.load(sqlResult.rows[0]);
        } else {
            let fieldSqlSection = '';
            let valuesSqlSection = '';
            let values : object[] = [];
            let indexParameterIncrement = 0;
            Object.keys(this.tableFields).forEach((key,index) => {
                if (key === this.pkFieldName) {
                    if (this.pkSequenceName) {
                        fieldSqlSection += key + ','
                        valuesSqlSection += `nextval('${this.pkSequenceName}'::regclass),`;
                    }
                } else {
                    fieldSqlSection += key + ','
                    indexParameterIncrement++;
                    valuesSqlSection += `$${indexParameterIncrement}` + ',';
                    values.push(this[key]);
                }
                
            });
            let sql = `INSERT INTO ${this.tableName}(${fieldSqlSection.slice(0,-1)}) 
                       VALUES (${valuesSqlSection.slice(0,-1)}) 
                       RETURNING * `;
            this.lastSql = sql;
            const sqlResult = await this._getPg(pginstance).query(sql, values);
            this.load(sqlResult.rows[0]);
        }
        return this;
    }


    load(row: Row): this {

        Object.assign(this, row);

        return this;

    }

    _getPg(pginstance?: PgHelper): PgHelper {
        // return db instance
        const pg = pginstance || this.pginstance;

        if (!pg) {
            throw 'pginstance empty';
        }
        return pg;
    }
}